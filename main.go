package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

var (
	publish      = flag.String("publish", "public", "Directory to serve content as root")
	port         = flag.Int("port", 8080, "Port for the http server")
	readTimeout  = flag.String("readTimeout", "3s", "Read timeout for the http server (10ms,10s,10m)")
	writeTimeout = flag.String("writeTimeout", "3s", "Write timeout for the http server (10ms,10s,10m)")
)

func main() {
	flag.Parse()

	r := mux.NewRouter()
	r.HandleFunc("/_/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("OK"))
	}).Methods(http.MethodGet)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir(*publish)))
	server := http.Server{
		ReadTimeout:  parseDurationWithDefault(*readTimeout, 3*time.Second),
		WriteTimeout: parseDurationWithDefault(*writeTimeout, 3*time.Second),
		Handler:      r,
		Addr:         fmt.Sprintf(":%d", *port),
	}
	log.Printf("Listening on port %d", *port)
	log.Println(server.ListenAndServe())
}

func parseDurationWithDefault(s string, d time.Duration) time.Duration {
	du, err := time.ParseDuration(s)
	if err != nil {
		log.Printf("Could not parse duration %s, fallback: %s", s, d)
		return d
	}
	return du
}
